const glob = require('glob')
const path = require('path')

const eta = require('eta')
eta.configure({
    views: __dirname
})

module.exports = {

    cache: {},

    async buildCache() {
        const paths = await glob.sync(path.resolve(__dirname, '*.{txt,html}'))
        this.cache = {}
        paths.forEach(f => {
            const filename = path.basename(f)
            const ext = filename.split('.').pop()
            const key = filename.slice(0, -1-ext.length)
            this.cache[key] = this.cache[key] || {}
            this.cache[key][ext] = async payload => {
                let finalPayload = {
                    logoUrlCb: process.env.APP_URL + '/img/logo-purple-240.png'
                }
                Object.assign(finalPayload, payload)
                return await eta.renderFile(filename, finalPayload);
            }
        })
    },

    /**
     *
     * @param name
     * @returns {null|{html: async Function, text: async Function}}
     */
    resolveCompiler(name)
    {
        const item = this.cache[name]
        if ( ! item) return null

        const r = {html: async ()=>null, text: async ()=>null}
        Object.assign(r, item)

        return r
    }

}