const Contact = require('./ContactMongo')

module.exports = {

    contact: {
        async saveEmail(email) {
            let model = await Contact.findOne({email}).exec()
            if (model) return model

            model = new Contact({email})
            return await model.save()
        }
    }

}