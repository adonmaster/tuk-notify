import {Model} from "mongoose";

export default class ContactMongo extends Model {
    email: string
    name?: string
    created_at: Date
}
