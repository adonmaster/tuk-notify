const mongoose = require('mongoose')
const Schema = mongoose.Schema

module.exports = mongoose.model('Contact', new Schema({

    email: {type: String, required: true, unique: true},
    name: {type: String},
    created_at: {type: Date, default: Date.now}

}))
