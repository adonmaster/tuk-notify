const mongoose = require('mongoose')

module.exports = {

    initialized: false,

    async init()
    {
        if (this.initialized) return
        this.initialized = true

        try {

            const options = {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                dbName: process.env.MONGO_DB_NAME,
                useFindAndModify:false,
                useCreateIndex: true,
                autoIndex: false
            }

            mongoose.connection.on('error', e => vlog.error(e.message, e))
            mongoose.connection.once('open', ()=> {
                vlog.log('mongo db connected')
                vlog.fix('mongo', 'connected')
            })

            vlog.log('initializing mongo connection')
            await mongoose.connect(process.env.MONGO_URL_CONNECTION, options)
        }
        catch(e) {
            vlog.error('mongodb init: ' + e.message)
            vlog.fix('mongo', e.message)
        }
    },

    async deinit() {
        if (this.initialized) return await mongoose.connection.close()
    }
}