const Validator = require('validatorjs')
const SESSender = require('../senders/SESSender')
const TemplateResolver = require('../template/TemplateResolver')
const Logger = require('../helpers/Logger')
const MongoRepo = require('../mongo/MongoRepo')

module.exports = async (json) => {

    const validator = new Validator(json, {
        'email': 'required|email',
        'template': 'required',
        'subject': 'required',
        'payload': 'required'
    })

    if (validator.fails()) {
        vlog.error(`erro de validação ${JSON.stringify(validator.errors.all())}`, )
        return false
    }

    const {email, template, subject, payload} = json
    const compiler = TemplateResolver.resolveCompiler(template)

    if ( ! compiler) {
        vlog.error("compiler not found: " + template)
        return false
    }

    try {
        const html = await compiler.html(payload)
        const text = await compiler.text(payload)
        let r
        if (process.env.APP_ENV === 'production') {
            r = await SESSender.send([email], subject, html, text)
        } else {
            r = {}
            vlog.log('emaling: ' + text + '<---------->' + html)
        }
        vlog.log(`email sent: ${JSON.stringify(r)}`)

        vlog.log('saving email to db')
        await MongoRepo.contact.saveEmail(email)

    } catch (e) {
        vlog.error(e.message, e)
        Logger.e(message, e)
        return false
    }


    return true
}