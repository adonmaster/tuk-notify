const capacity = 100
const cache = []
const fixed = []

module.exports = {

    fix(key, msg) {
        fixed[key] = {
            date: (new Date).toLocaleString(),
            msg
        }
    },

    log(msg, obj={}, type='log')
    {
        console.log(msg, obj)

        if (cache.length > capacity) cache.pop()
        cache.unshift({
            type, date: (new Date).toLocaleString(), msg
        })
    },

    error(msg, payload={}) {
        this.log(msg, payload, 'error')
    },

    render() {

        let fixedStr = Object.keys(fixed)
            .map(k => `<i>${fixed[k].date}:</i> <strong>${k}:</strong> <b>${fixed[k].msg}</b>`)
            .join('<br>')

        let cacheStr = cache
            .map(o => {
                const cl = o.type === 'error' ? 'red' : 'grey'
                return `<i>${o.date}:</i> ${o.type} | <b style="color: ${cl}">${o.msg}</b>`
            })
            .join('<br>')

        return `${fixedStr} <br><br> ${cacheStr}`
    }

}