const Logger = require('../helpers/Logger')
const amqp = require('amqplib');

/*
import

process.env.AMQP_HOST
process.env.AMQP_PORT
process.env.AMQP_USERNAME
process.env.AMQP_PASSWORD
process.env.QUEUE_NAME
*/

module.exports = {

    /**
     *
     * @param {QueueMessage} msg
     * @returns {JSON|null}
     * @private
     */
    _extractJsonFrom(msg) {
        if (! msg.content) return null
        try {
            const s = Buffer.from(msg.content).toString()
            return JSON.parse(s)
        } catch {
            return null
        }
    },

    /**
     *
     * @param {QueueMessage} msg
     * @param {Channel} channel
     * @returns {void|*|{deliveryTag, multiple}}
     * @private
     */
    _onMessage(msg, channel)
    {
        // retry
        if (msg.fields.redelivered && msg.fields.deliveryTag > 3) {
            Logger.e('cube-pending', msg)
            try {
                channel.ack(msg)
            } catch(err) {
                Logger.e(err.message, err)
            }
            return
        }

        // cb null
        const cb = this.onMessageCb
        if ( ! cb) return channel.ack(msg)

        // deal with this
        cb(msg, this._extractJsonFrom(msg))
            .then(()=>{
                try {
                    channel.ack(msg)
                } catch (err) {
                    Logger.e(err.message, err)
                }
            })
            .catch(err => {
                Logger.e(`nack err: ${err}`, msg)
                channel.nack(msg)
            })
    },

    onMessageCb: null,

    async init()
    {
        const that = this

        try {


            const conn = await amqp.connect({
                protocol: 'amqps',
                hostname: process.env.AMQP_HOST,
                port: process.env.AMQP_PORT,
                username: process.env.AMQP_USERNAME,
                password: process.env.AMQP_PASSWORD
            })
            vlog.log('amqp connected...')

            // close gracefully
            process.once('SIGINT', ()=> conn.close())

            //
            const channel = await conn.createChannel()
            vlog.log('amqp channel created...')

            // creating the main queue
            const queueName = process.env.QUEUE_NAME
            await channel.assertQueue(queueName, {durable: true})
            vlog.log(`amqp queue (${queueName}) asserted...`)

            const exchange = 'amq.direct'
            channel.bindQueue(queueName, exchange, queueName)
            vlog.log(`amqp queue (${queueName}) binded to (${exchange})...`)

            await channel.consume(queueName, msg=>{ that._onMessage(msg, channel) }, {noAck: false})

            vlog.fix('queue', 'connected')
        } catch (e) {
            Logger.e(e.message, e)
            vlog.fix('queue', e)
        }
    }
}