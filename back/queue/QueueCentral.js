const transactionalJob = require('../jobs/transactional-job')

/**
 *
 * @param {QueueMessage} msg
 * @param {JSON} json
 * @returns {boolean}
 */
module.exports = async (msg, json) =>
{
    if (! json) {
        vlog.error('Invalid Json: ' + Buffer.from(msg.content).toString())
        return false
    }

    const action = json['action'];

    if (action === 'transactional') {
        await transactionalJob(json)
    } else {
        vlog.error('no action recognized')
    }

    return true
}