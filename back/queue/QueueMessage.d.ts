import * as Buffer from "buffer";


interface QueueMessage {
    fields: {
        consumerTag: string
        deliveryTag: number,
        redelivered: boolean,
        exchange: string,
        routingKey: string
    },
    properties: {
        contentType?: string|'application/json',
        contentEncoding?: string,
        headers: {[key: string]: string},
        deliveryMode: number,
        priority?: string,
        correlationId?: string,
        replyTo?: string,
        expiration?: any,
        messageId?: number,
        timestamp?: number,
        type?: string,
        userId?: number,
        appId?: any,
        clusterId?: string
    },
    content: Uint8Array|string
}
