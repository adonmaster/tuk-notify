const aws = require('aws-sdk')
aws.config.update({region: 'us-east-2'})

module.exports = {

    /**
     *
     * @param {string[]} to
     * @param {string} subject
     * @param {string} html
     * @param {string} text
     */
    async send(to, subject, html, text)
    {
        const ses = new aws.SESV2()
        const params = this._generateParams(to, subject, html, text)
        return await ses.sendEmail(params).promise()
    },

    /**
     * @param {string[]} to
     * @param {string} subject
     * @param {string} html
     * @param {string} text
     * @returns {SESV2.Types.SendEmailRequest}
     * @private
     */
    _generateParams(to, subject, html, text)
    {
        const Body = {}
        if (html) Body['Html'] = {Data: html}
        if (text) Body['Text'] = {Data: text}
        return {
            FromEmailAddress: process.env.MAIL_FROM,
            Destination: {
                ToAddresses: to
            },
            Content: {
                Simple: {
                    Body,
                    Subject: { Data: subject }
                }
            }
        }
    }

}