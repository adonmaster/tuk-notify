const {should, expect} = require('chai')
should()

describe('TransactionalJob', () => {

    before(async ()=>{
    })

    it('should assert simple', () => {
        expect(1).to.be.equal(1)
    })

    it('should validate', async () => {
        await require('../back/template/TemplateResolver').buildCache()

        const job = require('../back/jobs/transactional-job')
        const r = await job({})
        expect(r).to.be.false
    })

    it('should do the thing', async () => {
        global.vlog = {error: ()=> {}}
        await require('../back/template/TemplateResolver').buildCache()
        const job = require('../back/jobs/transactional-job')
        const json = {
            email: 'adon@adon.com', template: 'test-1', code: '3452432', subject: 'my hands', payload: {}
        }
        const r = await job(json)
        expect(r).to.be.true
    })



})