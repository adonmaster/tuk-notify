const {should, expect} = require('chai')
should()

describe('TemplateResolver', () => {

    it('should assert simple', () => {
        expect(1).to.be.equal(1)
    })

    it('should validate', async () => {
        const resolver = require('../back/template/TemplateResolver')
        await resolver.buildCache()
        const compiler = resolver.resolveCompiler('send-code')
        expect(compiler).to.be.not.null
    })



})