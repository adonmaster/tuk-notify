// env
require('dotenv').config()

// log
require('./back/helpers/Logger').init()
global.vlog = require('./back/helpers/VirtualLog')

// express
const express = require('express')
const app = express()

// build cache for template .eta.html
require('./back/template/TemplateResolver').buildCache()

// db
const Mongo = require('./back/mongo/Mongo')
Mongo.init()

// Queue
const Queue = require('./back/queue/Queue')
Queue.onMessageCb = require('./back/queue/QueueCentral')
Queue.init()

// -----------# Routing #-----------
const path = require('path')
app.use(express.static(path.resolve(__dirname, 'public')))

app.use('/', (_, res) => {
    let html = `<b>cube-ms-notify</b> v${process.env.APP_VERSION} - ${(new Date).toLocaleString()}<br><br>`
    html = html + vlog.render()
    const fullHtml = `<html lang="pt-br"><body style="background-color: black; color: white">${html}</body></html>`
    res.send(fullHtml)
})
// ---------------------------------



// terminating
app.once('SIGUSR2', () => {
    Mongo.deinit()
    process.kill(process.pid, 'SIGUSR2')
})

// listening
const port = process.env.PORT || 4003
const baseUrl = process.env.APP_URL || 'http://localhost'
app.listen(port, () => console.log(`Listening to ${baseUrl}:${port}`))